

// - If the url is http://localhost:4000/, send a response Welcome to Booking System - GET
// - If the url is http://localhost:4000/profile, send a response Welcome to your profile! -GET
// - If the url is http://localhost:4000/courses, send a response Here’s our courses available - GET
// - If the url is http://localhost:4000/addcourse, send a response Add a course to our resources - POST
// - If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources - PUT
// - If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources - DELETE




let http = require("http");

let port = 4000;

let server = http.createServer((req, res) => { 

	//HTTP METHOD - GET

	if(req.url == "/" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to Booking System");
	}else if(req.url == "/profile" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to your profile!");
	}else if(req.url == "/courses" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Here’s our courses available");
	}


	//HTTP METHOD - POST

	if(req.url == "/addcourse" && req.method == "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Add a course to our resources");
	}


	//HTTP METHOD - PUT

	if(req.url == "/updatecourse" && req.method == "PUT"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Update a course to our resources");
	}


	//HTTP METHOD - DELETE

	if(req.url == "/archivecourses" && req.method == "DELETE"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Archive courses to our resources");
	} 

});

server.listen(port);

console.log(`Server is running at localhost:${port}`);